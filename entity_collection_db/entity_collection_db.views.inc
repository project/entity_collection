<?php

/**
 * Implements hook_views_data().
 */
function entity_collection_db_views_data() {
  $data = array();

  /**
   * Node-only integration
   */
  if (module_exists('node')) {

    $data['entity_collection_storage']['table'] = array(
      'join' => array(
        'node' => array(
          'left_field' => 'nid',
          'field' => 'eid',
          'extra' => array(
            array(
              'field' => 'entity_type',
              'value' => 'node',
            ),
          ),
        ),
      ),
    );

    $data['entity_collection_storage']['position'] = array(
      'group' => t('Entity Collection'),
      'title' => t('Position in a Collection'),
      'help' => t('Please note that this value without "Depth" makes sense only for flat (max depth: 0) collections.'),
      'field' => array(
        'table'=> 'entity_collection_storage',
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
        'field_name' => 'position'
      ),
      'filter' => array(
        'table'=> 'entity_collection_storage',
        'handler' => 'views_handler_field_numeric',
        'field_name' => 'position'
      ),
      'sort' => array(
        'table'=> 'entity_collection_storage',
        'handler' => 'views_handler_sort',
        'field_name' => 'position'
      ),
    );

    $data['entity_collection_storage']['depth'] = array(
      'group' => t('Entity Collection'),
      'title' => t('Depth in a Collection'),
      //      'help' => t(''),
      'field' => array(
        'table'=> 'entity_collection_storage',
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
        'field_name' => 'depth'
      ),
    );

  }

  return $data;

}

/**
 * Implements hook_views_data_alter().
 */
function entity_collection_db_views_data_alter(&$data) {

  $data['node']['entity_collection'] = array(
    'relationship' => array(
      'title' => t('Entity Collection'),
      'label' => 'entity collection',
      'help' => t('Connects to an Entity Collections'),
      'base' => 'entity_collection_storage',
      'base field' => 'eid',
      'field' => 'nid',
      'handler' => 'views_handler_relationship_entity_collection',
      'extra' => array(
        array(
          'field' => 'entity_type',
          'value' => 'node',
        ),
      ),
    ),
  );

}
