<?php
/**
 * @file
 * Relationship for Entity Collection handler.
 */

class views_handler_relationship_entity_collection extends views_handler_relationship {

  function option_definition() {
    $options = parent::option_definition();

    $options['entity_collection_name'] = array('default' => -1);

    return $options;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $collection_types = array(
      -1 => 'Any',
    );
    foreach (entity_collection_load_all() as $name => $collection) {
      $collection_types[$name] = $collection->title;
    }

    $form['entity_collection_name'] = array(
      '#type' => 'select',
      '#title' => t('Optional: Connect to a specific Collection'),
      '#description' => t("Note: Setting this will override any 'Filter' on entity collction."),
      '#options' => $collection_types,
      'select_text' => array('#attributes'=>array('disabled' => TRUE)),
      '#default_value' => $this->options['entity_collection_name'],
    );
  }

  function query() {
    parent::query();

    $ec_storage_table = &$this->query->table_queue[$this->alias];

    if (isset($this->options['entity_collection_name']) && $this->options['entity_collection_name']) {
      $ec_storage_table['join']->extra[] = array(
        'field' => 'name',
        'value' => $this->options['entity_collection_name'],
      );
    }
  }


}
