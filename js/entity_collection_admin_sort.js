(function ($) {
  if (Drupal.tableDrag) {
    
    Drupal.behaviors.entity_collection_admin_sort = {
      attach: function (context, settings) {
        $('table tr.draggable').each(function(index) {
          var $row = $(this);
          $row.once(this, function() {
            var $checkbox = $row.find('input.position-lock');
            $checkbox.change(function(event) {
              var classAction = ($checkbox.is(':checked')) ? 'addClass' : 'removeClass';
              $row[classAction]('position-lock');
              $row.find('.form-item input.position').attr('readonly', ($checkbox.is(':checked')));
            });
            // triggger the event the first time
            $checkbox.change();
          })
        });
      }
    };


    /**
     * Injects the "validSwap" logic to deny the movement of locked rows
     */ 
    Drupal.tableDrag.prototype.row.prototype.isValidSwap = function (isValidSwap) {
      return function (testRow) {
        // do not allow swap of locked rows.
        if ( $(testRow).hasClass('position-lock') || $(this.element).hasClass('position-lock') ) {
          return false;
        }
        // Run the standard function if our test are passing
        return isValidSwap.call(this, testRow);
      };
    }(Drupal.tableDrag.prototype.row.prototype.isValidSwap);


    /**
     * If required swaps extra rows to be coherent with the lock status
     */
    Drupal.tableDrag.prototype.row.prototype.onSwap = function(swappedElement) {
      var self = this;
      var checkPosition = (self.direction == 'down') ? 'prev' : 'next';
      var swapDirection = 'after';
      var $sibling = $(swappedElement)[checkPosition]('.position-lock');

      // If the top/bottom sibling of the swappedElement is locked we must swap 
      // another item above or below it to keep the coherent lock sorting.
      if ( !self.hasLockedSib(checkPosition) && $sibling.size() > 0) {
        // Find the first non-locked item 
        var $notLockedSibiling = $sibling;
        do {
          $notLockedSibiling = $notLockedSibiling[checkPosition]();
        } while ( !($notLockedSibiling.is(':not(.position-lock)') || $notLockedSibiling.size() <= 0) );

        // Fallback to the first or last if we reach the limit of the list
        if ( $notLockedSibiling.size() <= 0 ) {
          var fallbackFrom = (self.direction == 'down') ? 'first' : 'last' ;
          swapDirection = (fallbackFrom == 'first') ? 'before' : 'after' ;
          $notLockedSibiling = $(swappedElement).siblings()[fallbackFrom]();
        }

        var swappedRow = new this.constructor(swappedElement, 'position-lock', self.indentEnabled, self.maxDepth, false);
        swappedRow.swap(swapDirection, $notLockedSibiling.get(0));
      }
      else {
        console.log("Skip auto-swap because of locked "+checkPosition+" sibling on drag action");
      }

      // Always update the siblings status after a swap action!
      self.setLockedSibilings();
      return null;
    };


    /**
     * As soon as the draag starts it loads the info about the siblings lock status
     */
    Drupal.tableDrag.prototype.onDrag = function() {
      if ( this.rowObject ) {
        this.rowObject.setLockedSibilings()
      }
      return null;
    }


    /**
     * Removes any info about the siblings after the swapping is done.
     */
    Drupal.tableDrag.prototype.onDrop = function() {
      if (this.rowObject._locked_sibilings) {
        delete this.rowObject._locked_sibilings;
      }
    }


    /**
     * Saves the state of the siblings on the row
     */
    Drupal.tableDrag.prototype.row.prototype.setLockedSibilings = function() {
      self = this;

      $item = $(self.element);
      self.locked_sibilings = {
        prev: $item.prev().hasClass('position-lock'),
        next: $item.next().hasClass('position-lock')
      };
    }


    /**
     * Checks if any sibiling was locked
     */
    Drupal.tableDrag.prototype.row.prototype.hasLockedSib = function(side) {
      if ( this.locked_sibilings == undefined ) {
        this.setLockedSibilings();
      }
      if ( this.locked_sibilings[side] != undefined ) {
        return this.locked_sibilings[side];
      }

      return null;
    }


  } // if (Drupal.tableDrag)
})(jQuery);
